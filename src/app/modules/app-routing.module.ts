import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from '../views/aboutus/aboutus.component';
import { ContactsusComponent } from '../views/contactsus/contactsus.component';
import { HomeComponent } from '../views/home/home.component';
import { AdmissionComponent } from '../views/admission/admission.component';
import { ResultComponent } from '../views/result/result.component';
import { NewsEventComponent } from '../views/news-event/news-event.component';
import { ClassessComponent } from '../views/classess/classess.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'contactus', component: ContactsusComponent },
  { path: 'admission', component: AdmissionComponent },
  { path: 'result', component: ResultComponent },
  { path: 'news-event', component: NewsEventComponent },
  { path: 'classes', component: ClassessComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

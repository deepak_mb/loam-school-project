import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeadersComponent } from './components/headers/headers.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { HomeComponent } from './views/home/home.component';
import { AboutusComponent } from './views/aboutus/aboutus.component';
import { ContactsusComponent } from './views/contactsus/contactsus.component';

import { AppRoutingModule } from './modules/app-routing.module';
import { AdmissionComponent } from './views/admission/admission.component';
import { NewsEventComponent } from './views/news-event/news-event.component';
import { ResultComponent } from './views/result/result.component';
import { ClassessComponent } from './views/classess/classess.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    CarouselComponent,
    HomeComponent,
    AboutusComponent,
    ContactsusComponent,
    AdmissionComponent,
    NewsEventComponent,
    ResultComponent,
    ClassessComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
